#!/usr/bin/make -f
# -*- makefile -*-

#export DH_VERBOSE=1

DEB_BUILD_MULTIARCH=$(shell dpkg-architecture -qDEB_BUILD_MULTIARCH)

export OMPI_MCA_orte_rsh_agent=/bin/false

%: 
	dh $@ --builddirectory=builddir --with python3

override_dh_auto_clean:
	dh_auto_clean --builddirectory=builddir
	find $(CURDIR) -name "*.pyc" -delete
	$(RM) -r $(CURDIR)/builddir/tmp-scratch

override_dh_auto_configure:
	dh_auto_configure --  				\
		-DCMAKE_BUILD_TYPE=RelWithDebInfo	\
		-DPYTHON_EXECUTABLE="/usr/bin/python3"	\
		-DENABLE_XHOST=0			\
		-DENABLE_CheMPS2=ON

override_dh_auto_install:
	dh_auto_install
	mkdir -p debian/tmp/usr/share/doc/psi4/examples
	$(RM) debian/tmp/usr/share/psi/databases/*.pyc
	find debian/tmp/usr/share/psi/python -name "*.pyc" | xargs rm -f
	cp samples/example_psi4rc_file debian/tmp/usr/share/doc/psi4/psi4rc.example
	for i in tu1-h2o-energy tu2-ch2-energy tu3-h2o-opt tu4-h2o-freq \
	tu5-sapt adc1 castup1 scf1 scf2 scf3 scf5 scf-guess-read dft1 dfmp2-1 \
	cc1 cc2 cc3 cc4 cc8 cc8a cc12 \
	cc15 cc16 cc17 cc18 cc19 cc23 cc24 cc28 cc32 cc49 \
	fci-dipole cisd-h2o+-0 ci-multi \
	cisd-opt-fd rasci-h2o mpn-bh \
	mcscf1 sapt1 sad1 \
	psimrcc-sp1 mints1 mints2 matrix1 opt1 opt1-fd gibbs omp2-2 \
	omp3-1 omp2p5-2 ocepa1 fnocc2 fnocc4 \
	mp2p5-grad1 ocepa-grad2 omp2-grad1 \
	dcft1 dcft2 dcft3 dcft4 pywrap-db1 pywrap-db2 pywrap-cbs1 pywrap-all pywrap-alias \
	pywrap-checkrun-rhf pywrap-freq-e-sowreap; \
		do cp samples/$$i/input.dat \
		$(CURDIR)/debian/tmp/usr/share/doc/psi4/examples/$$i.dat; \
	done

override_dh_install:
	dh_install
	find . -name __pycache__ -type d | xargs rm -rf
	for sh in `grep -Rl '#!/usr/bin/bash' debian/*/usr/*` ; do \
	    sed -i '1s?^#!/usr/bin/bash?#!/bin/bash?' $${sh} ; \
	done

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
override_dh_auto_test:
	mkdir -p $(CURDIR)/builddir/tmp-scratch
	-(cd builddir/tests; CTEST_OUTPUT_ON_FAILURE=TRUE ctest -L quicktests)
endif

override_dh_python3:
	dh_python3 /usr/lib/$(DEB_BUILD_MULTIARCH)/psi4/
